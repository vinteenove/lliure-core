#lliure-core
[![license]](https://bitbucket.org/vinteenove/lliure-core/src/master/LICENSE)
![stable-version]
![last-version]

[license]: https://img.shields.io/packagist/l/lliure/lliure-core?style=for-the-badge
[stable-version]: https://img.shields.io/packagist/v/lliure/lliure-core?label=stable&style=for-the-badge
[last-version]: https://img.shields.io/packagist/v/lliure/lliure-core?label=last&style=for-the-badge&include_prereleases
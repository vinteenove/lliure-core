<?php

namespace LliureCore;

/**
 * @property array $queryParameter
 * @property array $orderParameter
 * @property array $entriesParameter
 * @property array $pageParameter
 * @property int   $entriesDefault
 */
trait Listing
{
	private static array $OPERATIONS = [
		"E"   => "=", "!E" => "!=",
		"B"   => "<", "BE" => "<=",
		"A"   => ">", "AE" => ">=",
		"IS"  => "IS", "!IS" => "NOT IS",
		"IN"  => "IN", "!IN" => "NOT IN",
		'L'   => 'LIKE', '!L' => 'NOT LIKE',
		'LL'  => '%LIKE', '!LL' => '%NOT %LIKE',
		'RL'  => 'LIKE%', '!RL' => 'NOT LIKE%',
		'LRL' => '%LIKE%', '!LRL' => 'NOT %LIKE%',
		'S'   => 'SEARCH',
	];
	
	protected static function getQueryParameter(): array|string
	{
		return self::$queryParameter ?? ['query', 'q'];
	}
	
	protected static function getOrderParameter(): array|string
	{
		return self::$orderParameter ?? ['order', 'o'];
	}
	
	protected static function getEntriesParameter(): array|string
	{
		return self::$entriesParameter ?? ['entries', 'e'];
	}
	
	protected static function getPageParameter(): array|string
	{
		return self::$pageParameter ?? ['page', 'p'];
	}
	
	public static function getEntriesDefault(?int $default = 20): ?int
	{
		return self::$entriesDefault ?? $default;
	}
	
	private static function findParameter(string|array $needle, array $haystack, mixed $defalt = null): mixed
	{
		foreach((array) $needle as $key) if(array_key_exists($key, $haystack)) return $haystack[$key];
		return $defalt;
	}
	
	private static function dataWhere(array $parans): ModelWhereInterface
	{
		
		$query = self::findParameter(static::getQueryParameter(), $parans, []);
		$where = self::whereBuider();
		$fieldsFind = (self::$fieldsFind ?? []);
		$ops = array_keys(static::$OPERATIONS);
		
		$add = fn($c, $o, &$v) => [$c, ((is_array($v) && (count($v) === 1))? array_shift($v): $v), $o];
		
		$values = [];
		foreach($query as $col => $v){
			if($fieldsFind && !in_array($col, $fieldsFind)) continue;
			
			if(is_array($v)){
				$val = [];
				$ope = null;
				
				foreach($v as $o => $i){
					$o = strtoupper(trim($o));
					
					if($ope !== null && in_array($o, $ops) && $ope != $o){
						$values[] = $add($col, $ope, $val);
						$ope = null;
						$val = [];
					}
					
					$val[] = $i;
					if(in_array($o, $ops)){
						$ope = $o;
					}
				}
				
				if(!empty($val) && $ope === null){
					$ope = "E";
				}
				
				if($ope !== null){
					$values[] = $add($col, $ope, $val);
				}
				
				continue;
			}
			
			$values[] = $add($col, 'E', $v);
		}
		
		foreach($values as [$c, $v, $o]){
			$o = static::$OPERATIONS[$o];
			
			$v = (!in_array($o, ["IS", "NOT IS"]))? $v: (($v === null || $v === "" || strtoupper((string) $v) === "NULL")? null: !!$v);
			
			$c = preg_split('/(;|\|)+/', $c, flags: PREG_SPLIT_NO_EMPTY);
			$c = ((count($c) === 1)? array_shift($c): $c);
			
			if($o == "SEARCH"){
				$where = self::search($where, $c, $v);
				
			}elseif(is_string($c)){
				$where->set($c, $v, $o);
				
			}elseif(is_array($c)){
				$where->set(function(ModelWhereInterface $where) use($c, $v, $o){
					foreach($c as $col){
						$where->or($col, $v, $o);
					}
					return $where;
				});
			}
		}
		
		return $where;
	}
	
	private static function dataOrder(array $parans, array $default): array
	{
		$orderParam = self::findParameter(static::getOrderParameter(), $parans, []);
		$order = [];
		if(is_array($orderParam)) foreach($orderParam as $col => $dir){
			!in_array($dir, ['DESC', 'desc', '0'], true)?: $order[$col] = 'DESC';
			!in_array($dir, ['ASC', 'asc', '1'], true)?: $order[$col] = 'ASC';
		}
		
		return (empty($order)? $default: $order);
	}
	
	private static function dataEntries(array $parans, ?int $default): int
	{
		$entries = self::findParameter(static::getEntriesParameter(), $parans, $default);
		$infinite = ($entries !== NULL && ($entries == 'all' || $entries <= 0));
		return ($infinite? 0: min(1000, max(1, ((int) $entries))));
	}
	
	private static function dataPages(array $parans, int $default): int
	{
		return self::findParameter(static::getPageParameter(), $parans, $default);
	}
	
	public static function searchProcess(array $params, ?ModelWhereInterface $where = null, array $order = [], ?int $entries = null, int $page = 1): array
	{
		$where = $where ?? self::whereBuider();
		$where->set(fn() => self::dataWhere($params));
		$order = self::dataOrder($params, $order);
		$entries = self::dataEntries($params, ($entries ?? static::getEntriesDefault()));
		$page = self::dataPages($params, $page);
		
		return [$where, $order, $entries, $page];
	}
	
	public static function listing(array $fields, ModelWhereInterface $where, array $order, int $entries, int $page = 1): array
	{
		$infinity = ($entries <= 0);
		$total = self::countByWhere($where);
		$pages = ($infinity? 1: ceil($total / $entries));
		$page = max(1, min($pages, $page));
		$postsData = self::findByWhere($fields, $where, $order, ($infinity? $total: $entries), ($page - 1));
		
		return ([
			'list'       => $postsData,
			'total'      => $total,
			'pagination' => [
				'entries'        => $entries,
				'order'          => $order,
				'totalPages'     => $pages,
				'currentPage'    => $page,
				'isInitial'      => ($page <= 1),
				'isEnd'          => ($page >= $pages),
				'totalPrevPages' => ($page - 1),
				'totalNextPages' => ($pages - $page),
			],
		]);
	}
	
	protected static function search(ModelWhereInterface $where, array|string $col, $val): ModelWhereInterface
	{
		$search = [];
		
		$col = ((!is_array($col))? (array) $col: $col);
		$val = ((!is_array($val))? (array) $val: $val);
		
		foreach($val as $v) $search = array_merge($search, preg_split("/\s+/", $v, flags: PREG_SPLIT_NO_EMPTY));
		$search = array_unique($search);
		
		$where->set(function(ModelWhereInterface $where) use($col, $search){
			foreach($search as $v){
				$where->and(function(ModelWhereInterface $where) use($col, $v){
					foreach($col as $c){
						$where->or($c, $v, '%LIKE%');
					}
					return $where;
				});
			}
			return $where;
		});
		
		return $where;
	}
}
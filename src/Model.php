<?php

namespace LliureCore;

use ArrayAccess;
use dibi;
use Exception;
use Helpers\ArrayHelper;
use Helpers\StringHelper;
use JsonSerializable;

use function json_decode;
use function json_encode;

abstract class Model implements ArrayAccess, JsonSerializable
{
    use ModelWhereBuider;

    /**
     * @var string[]
     */
    protected static array $fields = [];

    /**
     * @var string[]
     */
    protected static array $jsonFields = [];

    /**
     * @var array|string
     */
    protected static $primaryKey = 'id';

    /**
     * @var string
     */
    protected static ?string $autoIncrementColumn = null;

    /**
     * @var string|null
     */
    protected static ?string $table = null;

    /**
     * @var string
     */
    protected static ?string $prefix = null;

    /**
     * @var array
     */
    private static array $bootClassFlags = [];

    /**
     * @var array
     */
    private array $data = [];

    /**
     * @var array|null
     */
    private ?array $oldData = null;

    /**
     * @param string $prefix
     */
    public static function setPrefix(?string $prefix): void
    {
        static::$prefix = $prefix;
    }

    /**
     * @return string
     */
    public static function getPrefix(): ?string
    {
        return static::$prefix;
    }

    /**
     * @param int|null $page Page number, starting with 1
     * @param int $rowsPerPage
     *
     * @return Collection
     * @throws Exception
     */
    public static function all(?int $page = null, int $rowsPerPage = 10): Collection
	{
        static::boot();
		
        if (!is_null($page) && $page >= 1 && $rowsPerPage >= 1){
            $offset = ($page - 1) * $rowsPerPage;
        }else{
			$page = $offset = null;
		}
		
        return static::findMany('select * from %SQL %lmt %ofs', static::getTable(), $page, $offset);
    }

    /**
     * @param array $data
     * @return static
     */
    public static function build(array $data = [])
    {
        static::boot();

        return new static($data);
    }

    /**
     * @return int
     */
    public static function count(): int
    {
        static::boot();

        $count = 0;

        try {
            $result = dibi::query('SELECT count(*) AS rows_count FROM %SQL', static::getTable());
            $count = $result->fetch()->rows_count;
        } catch (Exception $exception) {
            //do nothing
        }

        return $count;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public static function filterFields(array $data): array
    {
        static::boot();

        if (empty(static::$fields)) {
            return $data;
        }

        return array_intersect_key($data, array_flip(static::$fields));
    }

    /**
     * Finds one row that will be represented by the current model.
     *
     * @param string $query Query must return all fields for the model
     * @param mixed ...$queryParams
     *
     * @return static|null
     * @throws Exception
     */
    public static function findOne($query, ...$queryParams): ?static
    {
        static::boot();

        $model = null;

        $result = dibi::query($query, ...$queryParams);

        if ($result->count()) {
            $model = new static($result->fetch()->toArray());
        }

        return $model;
    }

    /**
     * Finds rows that will be represented by the current model.
     *
     * @param string $query Query must return all fields for the model
     * @param mixed ...$queryParams
     *
     * @return Collection
     * @throws Exception
     */
    public static function findMany($query, ...$queryParams): Collection
    {
        static::boot();

        $models = new Collection();

        $result = dibi::query($query, ...$queryParams);

        while($row = $result->fetch()) {
            $models->add(new static($row->toArray()));
        }

        return $models;
    }

    /**
     * @return static|null
     * @throws Exception
     */
    public static function first(): ?static
    {
        static::boot();
        return static::findOne('SELECT * FROM %SQL LIMIT 1', static::getTable());
    }

    /**
     * @return array
     * @throws Exception
     */
    public static function getPrimaryKeys(): array
    {
        static::boot();

        return static::$primaryKey;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public static function getTable(): ?string
    {
        static::boot();
        return dibi::getConnection()->translate('%n', static::$prefix.static::$table);
    }

    /**
     * @param $id
     * @return static|null
     * @throws Exception
     */
    public static function load($id): ?static
    {
        static::boot();

        $pkBindings = static::createPrimaryKeyQueryBindings($id);
		
        return static::findOne('SELECT * FROM %SQL WHERE ' . $pkBindings['whereClause'], static::getTable(), ...$pkBindings['bindings']);
    }

    /**
     * Try to load. Return a new instance when not found.
     *
     * @param $id
     *
     * @return static
     * @throws Exception
     */
    public static function loadOrNew($id): static
    {
        static::boot();

        $instance = static::load($id);

        if (!$instance) {
            $instance = static::build();
        }

        return $instance;
    }

    /**
     * @return void
     * @throws Exception
     */
    private static function boot(): void
    {
        // Booted already?
        if (isset(static::$bootClassFlags[static::class])) {
            // Skip :)
            return;
        }

        // Set flag for future verification
        static::$bootClassFlags[static::class] = true;

        // Begin boot
        static::$primaryKey = (array) static::$primaryKey;

        if (empty(static::$table)){
            throw new Exception('You have to fill $table attribute');
        }
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function copy(): static
    {
        $data = $this->toArray();

        foreach (static::getPrimaryKeys() as $key) {
            unset($data[$key]);
        }

        return static::build($data);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function delete(): bool
    {
        $this->fillOldDataFromSourceTable();
        $bindings = $this->createPrimaryKeyQueryBindingsWithCurrentValues();
		
        $result = dibi::query(('DELETE FROM %SQL WHERE ' . $bindings['whereClause']), static::getTable(), ...$bindings['bindings']);
        $deleted = $result->count() == 1;

        if ($deleted) {
            $this->oldData = [];
        }

        return $deleted;
    }

    /**
     * Verifica se o registro existe no banco.
     *
     * @return bool
     * @throws Exception
     */
    public function exists(): bool
    {
        $bindings = $this->createPrimaryKeyQueryBindingsWithCurrentValues();
        $result = dibi::query(('SELECT 1 FROM %SQL WHERE ' . $bindings['whereClause']), static::getTable(), ...$bindings['bindings']);
        return $result->count() >= 1;
    }

    /**
     * Fetch primary key value.
     *
     * @return int|string|array|null
     */
    public function id(): int|array|string|null
    {
        if (empty(static::$primaryKey)) {
            return null;
        }

        if (count(static::$primaryKey) == 1) {
            $primaryKey = static::$primaryKey[array_key_first(static::$primaryKey)];
            return $this->$primaryKey;
        }

        $keys = [];
        foreach (static::$primaryKey as $key) {
            $keys[$key] = $this->$key;
        }
        return $keys;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function insert(): bool
    {
        try {
            $table = static::getTable();
			
            $data = $this->convertJsonFields($this->data);
			
            dibi::query('INSERT INTO %SQL %v', $table, $data);

            if (static::$autoIncrementColumn !== null){
                $this->data[static::$autoIncrementColumn] = dibi::getInsertId();
            }

            $this->oldData = $this->data;

            return true;
        } catch (\Throwable $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function update(): bool
    {
		$updateData = $this->convertJsonFields($this->data);
		$pk = array_flip($this->getPrimaryKeys());
		$data = array_diff_key($updateData, $pk);
		$key = array_intersect_key($updateData, $pk);
		
		\dibi::query('UPDATE %SQL SET %a WHERE %and', static::getTable(), $data, $key);
		
		$updated = dibi::getAffectedRows() == 1;
		
		$this->oldData = $updated? $this->data: $this->oldData;
		
		return $updated;
    }

    /**
     * Salva os dados em banco.
     *
     * @return bool
     * @throws Exception
     */
    public function save()/*: bool*/
    {
        try {
            $return = $this->insert();
        } catch (Exception $e) {
            $return = $this->update();
        }

        return $return;
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return array data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource.
     *
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->data[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->$offset;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->$offset = $value;
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->$offset);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }

    /**
     * Model constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        static::boot();

        //Convert json fields to array
        if (!empty($data)) {
            foreach (static::$jsonFields as $field) {
                if (!empty($data[$field]) && StringHelper::isJson($data[$field])) {
                    $data[$field] = json_decode($data[$field], true);
                }
            }
        }

        $this->data = $data;
    }
	
	/**
	 * Retorna uma instância da tabela onde cada coluna definida na variável estática `$fields`
	 * possui como valor o próprio nome da coluna. Isso permite que as colunas sejam referenciadas
	 * diretamente em queries SQL.
	 *
	 * @param string|null $alias Opcional. Prefixo para as colunas, usado em consultas SQL para desambiguar colunas.
	 *                            Caso fornecido, cada coluna será prefixada com o alias seguido de um ponto.
	 * @return static Uma instância da classe com os dados mapeados para o alias especificado.
	 */
	public static function col(?string $alias = null): static
	{
		
		$data = array_combine(static::$fields, static::$fields);
		
		if($alias){
			$data = array_map(fn($v) => ($alias . '.' . $v), $data);
		}
		
		return static::build($data);
	}
	
    /**
     * @param $name
     * @return mixed
     */
    public function &__get($name)
    {
        if (!isset($this->data[$name]) && in_array($name, static::$fields)) {
            $this->data[$name] = null;
        }

        return $this->data[$name];
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * @param $name
     * @param $value
     *
     * @throws Exception
     */
    public function __set($name, $value)
    {
        if (!is_array($value) && in_array($name, static::$jsonFields)) {
            throw new Exception("Field '$name' will only accept arrays");
        }

        $this->data[$name] = $value;
    }

    /**
     * @param $name
     */
    public function __unset($name): void
    {
        unset($this->data[$name]);
    }

    /**
     * @return array
     */
    protected function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array|null
     */
    protected function getOldData(): ?array
    {
        return $this->oldData;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function convertJsonFields(array $data): array
    {
        foreach (static::$jsonFields as $field) {
            if (isset($data[$field])) {
                if (empty($data[$field])) {
                    $data[$field] = [];
                }

                if (!is_array($data[$field]) && !($data[$field] instanceof JsonSerializable)) {
                    throw new Exception(
                        "Error while converting to json, field: " .
                        $field . "(". $data[$field] .")"
                    );
                }

                $data[$field] = json_encode($data[$field]);
            }
        }

        return $data;
    }

    /**
     * @throws Exception
     */
    private function fillOldDataFromSourceTable()
    {
        //not loaded yet?
        if (!is_null($this->oldData)) {
            return;
        }

        //guess it is a new object,
        //no data needs to be loaded
        if (!$this->id()) {
            $this->oldData = [];
            return;
        }

        //Load data
        $bindings = $this->createPrimaryKeyQueryBindingsWithCurrentValues();
        $result = dibi::query('SELECT * FROM %SQL WHERE ' . $bindings['whereClause'], static::getTable(), ...$bindings['bindings']);

        //does not exists in table
        if (!$result->count()) {
            $this->oldData = [];
            return;
        }

        //digest data to match object rules
        $oldObj = new static($result->fetch()->toArray());
        $this->oldData = $oldObj->toArray();
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getDataChanges(): array
    {
        $this->fillOldDataFromSourceTable();

        return ArrayHelper::array_diff_assoc_recursive($this->data, $this->oldData) ?: [];
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    private static  function createPrimaryKeyQueryBindings($id): array
    {
        $whereClause = '';
        $bindings = [];
        $pkCount = count(static::getPrimaryKeys());

        if (
            $pkCount == 0
            || (!is_array($id) && $pkCount > 1)
            || (is_array($id) && count($id) != $pkCount)
        ) {
            throw new Exception("Could not create primary key bindings");
        }

        if (!is_array($id) && $pkCount == 1) {
            $id =  [static::getPrimaryKeys()[0] => $id];
        }

        foreach (static::getPrimaryKeys() as $key) {
            if (!array_key_exists($key, $id)) {
                throw new Exception("$key must be an index in id parameter");
            }
			
			$whereClause .= " AND `$key` = ?";
            $bindings[] = $id[$key];
        }

        $whereClause = ltrim($whereClause, " AND");

        return compact('whereClause', 'bindings');
    }

    /**
     * @return array
     * @throws Exception
     */
    private function createPrimaryKeyQueryBindingsWithCurrentValues(): array
    {
        $ids = array_intersect_key(
            $this->getData(),
            array_flip(static::getPrimaryKeys())
        );

        return static::createPrimaryKeyQueryBindings($ids);
    }
}

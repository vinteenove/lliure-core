<?php

namespace LliureCore;

interface CollectionInterface extends \ArrayAccess, \JsonSerializable, \Countable, \Iterator
{
    /**
     * Adds an item.
     *
     * @param $item
     * @param string|int|null $index
     *
     * @return bool
     */
    public function add($item,string|int|null $index = null): bool;

    /**
     * Empty collection.
     *
     * @return void
     */
    public function clear(): void;

    /**
     * Creates an exact copy from this collection.
     *
     * @return static
     */
    public function copy(): static;

    /**
     * Cycles through items in the current collection.
     * $function must receive two arguments:
     * $value (mixed) - Will contain the current item in the iteration.
     * $key - Will contain the current item key.
     *
     * @param callable $function
     */
    public function each(callable $function);

    /**
     * Returns the value stored in $index.
     *
     * @param string $index
     *
     * @return mixed
     */
    public function get($index);

    /**
     * Gets the index from $item. Returns null when it doesn't exists.
     *
     * @param $item
     *
     * @return string|null
     */
    public function getIndex($item);

    /**
     * Checks if $item item exists in the current collection.
     *
     * @param $item
     *
     * @return bool
     */
    public function has($item);

    /**
     * Checks if $index was set.
     *
     * @param $index
     *
     * @return bool
     */
    public function hasIndex($index);

    /**
     * Return true when the collection is empty.
     *
     * @return bool
     */
    public function isEmpty();

    /**
     * Remove $item from this/child collections.
     *
     * @param mixed $item
     *
     * @return bool
     */
    public function remove($item);

    /**
     * @param string $index
     *
     * @return bool
     */
    public function removeIndex($index);

    /**
     * Sorts items in the current collection.
     * Based on usort. Please check documentation: http://php.net/manual/en/function.usort.php
     * $function callable notes: The comparison function must return an integer less than,
     * equal to, or greater than zero if the first argument is considered to be respectively less than,
     * equal to, or greater than the second.
     *
     * @param callable $function
     *
     * @return mixed
     */
    public function sort(callable $function);

    /**
     * Extracts the current collection into an array.
     *
     * @return array
     */
    public function toArray();

    /**
     * @param $name
     * @return mixed
     */
    public function &__get($name);

    /**
     * @param $name
     * @return mixed
     */
    public function __isset($name);

    /**
     * @param $name
     * @param $value
     * @return mixed
     */
    public function __set($name, $value);

    /**
     * @param $name
     */
    public function __unset($name);
}

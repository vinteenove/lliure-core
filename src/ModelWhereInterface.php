<?php

namespace LliureCore;


interface ModelWhereInterface
{

    public function set($collumn, $value = null, string $operador = '=');

    public function and($collumn, $value = null, string $operador = '=');

    public function or($collumn, $value = null, string $operador = '=');

    public function __toString():string;

}
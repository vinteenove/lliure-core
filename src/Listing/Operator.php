<?php

namespace LliureCore\Listing;

class Operator
{
	public Operator $not;
	protected $handler;
	
	/**
	 * @param callable(array):Query $handler
	 * @param Operator|null         $invert
	 */
	public function __construct(
		callable $handler,
		protected ?Operator $invert = null,
	){
		$this->handler = $handler;
		$this->not = ($invert)?: (new self($handler, $this));
	}
	
	protected function parameter($valeu): Query
	{
		return call_user_func($this->handler, $valeu);
	}
	
	public function equal(string $valeu): Query
	{
		return (!$this->invert)?
			$this->parameter($valeu):
			$this->parameter(['!E' => $valeu]);
	}
	
	public function before(string $valeu): Query
	{
		$o = ((!$this->invert)? "B": "AE");
		return $this->parameter([$o => $valeu]);
	}
	
	public function beforeEqual(string $valeu): Query
	{
		$o = ((!$this->invert)? "BE": "A");
		return $this->parameter([$o => $valeu]);
	}
	
	public function after(string $valeu): Query
	{
		$o = ((!$this->invert)? "A": "BE");
		return $this->parameter([$o => $valeu]);
	}
	
	public function afterEqual(string $valeu): Query
	{
		$o = ((!$this->invert)? "AE": "B");
		return $this->parameter([$o => $valeu]);
	}
	
	public function is(null|bool $valeu): Query
	{
		$o = ((!$this->invert)? "IS": "!IS");
		return $this->parameter([$o => $valeu]);
	}
	
	public function in(array $valeu): Query
	{
		if(empty($valeu)) $this->parameter(null);
		
		$o = ((!$this->invert)? "IN": "!IN");
		
		$valeu = array_values($valeu);
		$first = array_shift($valeu);
		
		return $this->parameter(array_merge([$o => $first], $valeu));
	}
	
	public function like(string $valeu): Query
	{
		$o = ((!$this->invert)? "L": "!L");
		return $this->parameter([$o => $valeu]);
	}
	
	public function likeLeft(string $valeu): Query
	{
		$o = ((!$this->invert)? "LL": "!LL");
		return $this->parameter([$o => $valeu]);
	}
	
	public function likeRight(string $valeu): Query
	{
		$o = ((!$this->invert)? "RL": "!RL");
		return $this->parameter([$o => $valeu]);
	}
	
	public function likeRightLeft(string $valeu): Query
	{
		$o = ((!$this->invert)? "LRL": "!LRL");
		return $this->parameter([$o => $valeu]);
	}
	
	public function search(string $valeu): Query
	{
		$o = ((!$this->invert)? "S": "!S");
		return $this->parameter([$o => $valeu]);
	}
	
}
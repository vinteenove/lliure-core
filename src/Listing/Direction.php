<?php

namespace LliureCore\Listing;

class Direction
{
	protected $handler;
	
	/**
	 * @param callable(array):Order $handler
	 */
	public function __construct(
		callable $handler,
	){
		$this->handler = $handler;
	}
	
	protected function parameter($valeu): Order
	{
		return call_user_func($this->handler, $valeu);
	}
	
	public function asc(): Order
	{
		return $this->parameter('asc');
	}
	
	public function desc(): Order
	{
		return $this->parameter('desc');
	}
}
<?php

namespace LliureCore\Listing;

use ArrayAccess;

class Query extends Params
{
	protected array $query;
	
	public function __construct(
		Params $context, array $parans = [],
	){
		$this->context($context);
		$this->query = $parans['q'] ?? $parans['query'] ?? [];
	}
	
	/**
	 * @param int|string $columns
	 * @return Operator
	 */
	public function and(...$columns): Operator
	{
		$columns = implode(';', $columns);
		return (new Operator(function($value) use ($columns){
			if($value === null) return $this;
			$this->query[$columns] = $value;
			return $this;
		}));
	}
	
	public function getQuery(): array
	{
		return $this->query;
	}
}
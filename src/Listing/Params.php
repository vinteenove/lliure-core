<?php

namespace LliureCore\Listing;

class Params
{
	const LIMIT_ALL = 0;
	
	protected ?self $context = null;
	
	private ?Query $query = null;
	private ?Order $order = null;
	
	private ?int $entries = null;
	private ?int $page = null;
	
	public function __construct(
		array $params = [],
	){
		$this->query = (new Query($this, $params));
		$this->order = (new Order($this, $params));
		$this->entries = $params['e'] ?? $params['entries'] ?? null;
		$this->page = $params['p'] ?? $params['page'] ?? null;
	}
	
	protected function context(self $context): void
	{
		$this->query =& $context->query;
		$this->order =& $context->order;
		$this->entries =& $context->entries;
		$this->page =& $context->page;
	}
	
	/**
	 * @param int|string $columns
	 * @return Operator
	 */
	public function query(...$columns): Operator
	{
		if($this->query == null) $this->query = new Query($this);
		return $this->query->and(...$columns);
	}
	
	/**
	 * @param string $colum
	 * @return Direction
	 */
	public function order(string $colum): Direction
	{
		if($this->order == null) $this->order = new Order($this);
		return $this->order->and($colum);
	}
	
	public function limit(int $entries, ?int $page = null): self
	{
		$this->entries = max(0, $entries);
		if($page !== null) $this->page = max(0, $page);
		return $this;
	}
	
	public function __debugInfo(): ?array
	{
		return [
			'query'   => $this->query?->getQuery(),
			'order'   => $this->order?->getOrder(),
			'entries' => $this->entries,
			'page'    => $this->page,
		];
	}
	
	public function params(): array
	{
		$params = [];
		
		$params['q'] = $this->query?->getQuery();
		if(empty($params['q'])) unset($params['q']);
		
		$params['o'] = $this->order?->getOrder();
		if(empty($params['o'])) unset($params['o']);
		
		if($this->entries !== null) $params['e'] = $this->entries;
		if($this->page !== null) $params['p'] = $this->page;
		
		return $params;
	}
}
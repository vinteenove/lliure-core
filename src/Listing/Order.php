<?php

namespace LliureCore\Listing;

class Order extends Params
{
	protected array $order;
	
	public function __construct(
		Params $context,
		array $parans = [],
	){
		$this->context($context);
		$this->order = $parans['o'] ?? $parans['order'] ?? [];
	}
	
	/**
	 * @param string $colum
	 * @return Direction
	 */
	public function and(string $colum): Direction
	{
		return (new Direction(function($value) use ($colum){
			$this->order[$colum] = $value;
			return $this;
		}));
	}
	
	public function getOrder(): array
	{
		return $this->order;
	}
}
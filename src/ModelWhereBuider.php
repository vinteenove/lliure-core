<?php

namespace LliureCore;

use Dibi\Connection;

trait ModelWhereBuider
{
	/**
	 * @return ModelWhereInterface
	 */
	public static function whereBuider()
	{
		return new class implements ModelWhereInterface {
			private array $whereArray = [];
			
			public function __construct(
				private ?\Closure $handle = null
			){}
			
			/**
			 * @param        $collumn
			 * @param null   $value
			 * @param string $operador
			 * @return ModelWhereInterface
			 */
			public function set($collumn, $value = null, $operador = '='): self
			{
				return $this->and($collumn, $value, $operador);
			}
			
			/**
			 * @param        $collumn
			 * @param null   $value
			 * @param string $operador
			 * @return ModelWhereInterface
			 */
			public function and($collumn, $value = null, $operador = '='): self
			{
				return $this->type('AND', $collumn, $operador, $value);
			}
			
			/**
			 * @param        $collumn
			 * @param null   $value
			 * @param string $operador
			 * @return ModelWhereInterface
			 */
			public function or($collumn, $value = null, $operador = '='): self
			{
				return $this->type('OR', $collumn, $operador, $value);
			}
			
			private function type($type, $collumn, $operador, $value): self
			{
				if($collumn instanceof \Closure){
					$this->whereArray[] = [$type, (new self($collumn))];
				}else{
					$this->whereArray[] = [$type, [$operador, self::escapeIdentifier($collumn), $value]];
				}
				
				return $this;
			}
			
			/**
			 * @param $whereArray
			 * @return string
			 */
			private function whereProcess($whereArray): string
			{
				$buffer = [];
				
				foreach($whereArray as $k => [$cmp, $ope]){
					if($ope instanceof self){
						if(!!($group = (string) ((($ope->handle)($ope)) ?? ((string) $ope)))){
							$buffer[] = $cmp;
							$buffer[] = '(' . $group . ')';
						}
						continue;
					}
					
					$buffer[] = $cmp;
					$buffer[] = $this->processeOperator($ope);
				}
				
				unset($buffer[0]);
				return implode(' ', $buffer);
			}
			
			/**
			 * @param $question
			 * @return string
			 */
			private function processeOperator($question): string
			{
				[$operator, $collumn, $value] = $question;
				
				$operator = strtoupper($operator);
				
				if(!in_array($operator, [
					"=", "==", "IS", "IN",
					">=", "<=", "!=", ">", "<",
					"NOT", "NOT IS", "NOT IN",
					'LIKE', '%LIKE', 'LIKE%', '%LIKE%',
					'NOT LIKE', 'NOT %LIKE', 'NOT LIKE%', 'NOT %LIKE%',
				])){
					throw new \Exception('Error: Operator ' . $operator . ' is Invalid ');
				}
				
				$operator = in_array($operator, ['==', 'IN', 'IS'])? '=': $operator;
				$operator = in_array($operator, ['NOT', 'NOT IN', 'NOT IS'])? '!=': $operator;
				$nega = !(in_array($operator, ['=', '<=', '>=', 'LIKE', '%LIKE', 'LIKE%', '%LIKE%']));
				
				$like =
					(in_array($operator, ['LIKE', 'NOT LIKE'])? null:
						(in_array($operator, ['%LIKE%', 'NOT %LIKE%'])? 0:
							(in_array($operator, ['%LIKE', 'NOT %LIKE'])? -1:
								(in_array($operator, ['LIKE%', 'NOT LIKE%'])? 1: false))));
				
				if(is_array($value)){
					$replace = (($nega? 'NOT ': '') . "IN %in");
				}elseif(is_null($value)){
					$replace = ('IS ' . ($nega? 'NOT ': '') . '?');
				}elseif(is_bool($value)){
					$replace = (($nega? '!': '') . '= ' . '%b');
				}elseif($like !== false){
					$replace = ($nega? 'NOT ': '') . 'LIKE ?';
					$value = ($like !== null && $like <= 0? '%': '') . ((string) $value) . ($like !== null && $like >= 0? '%': '');
				}else{
					$replace = ($operator . ' ?');
				}
				
				return \dibi::getConnection()->translate(($collumn . ' ' . $replace), $value);
			}
			
			private static function escapeIdentifier(string $collumn): string
			{
				return implode('.', array_map(function($coll){
					return \dibi::getConnection()->getDriver()->escapeIdentifier($coll);
				}, explode('.', $collumn)));
			}
			
			/**
			 * @return string
			 */
			public function __toString(): string
			{
				return $this->whereProcess($this->whereArray);
			}
		};
	}
	
	/**
	 * @param array               $collumns
	 * @param ModelWhereInterface $where
	 * @return static|null
	 */
	public static function findOneByWhere(array $collumns, ModelWhereInterface $where): ?static
	{
		$table = static::getTable();
		$collumns = empty($collumns)? ['*']: $collumns;
		
		$where = self::whereBuilder($where);
		
		return static::findOne(('SELECT %n FROM %SQL %SQL LIMIT 1'), $collumns, $table, $where);
	}
	
	/**
	 * @param array               $collumns
	 * @param ModelWhereInterface $where
	 * @param array|string        $order
	 * @param int                 $rowsPerPage
	 * @param int                 $page
	 * @return Collection
	 */
	public static function findByWhere(array $collumns, ModelWhereInterface $where, $order = [], $rowsPerPage = 50, $page = 0): Collection
	{
		$table = static::getTable();
		$collumns = empty($collumns)? ['*']: $collumns;
		$where = self::whereBuilder($where);
		$order = self::orderBuilder($order);
		$limit = self::limitBuilder($rowsPerPage, $page);
		
		return static::findMany(('SELECT %n FROM %SQL' . $where . $order . $limit), $collumns, $table);
	}
	
	/**
	 * @param ModelWhereInterface $where
	 * @return int
	 */
	public static function countByWhere(ModelWhereInterface $where): int
	{
		try{
			$table = static::getTable();
			$where = self::whereBuilder($where);
			$result = \dibi::query(('SELECT SUM(1) AS rows_count FROM %SQL' . $where), $table);
			return (int) $result->fetch()->rows_count;
		}catch(\Exception $exception){
			return 0;
		}
	}
	
	/**
	 * @param ModelWhereInterface $where
	 * @return bool
	 */
	public static function deleteByWhere(ModelWhereInterface $where): bool
	{
		$table = static::getTable();
		$where = self::whereBuilder($where);
		return !!(static::findOne(('DELETE FROM %SQL' . $where), $table) ?? false);
	}
	
	private static function whereBuilder($where): string
	{
		$where = (string) $where;
		return ($where? ' WHERE ' . $where: '');
	}
	
	private static function orderBuilder($order): string
	{
		$orderReturn = '';
		if(!empty($order)){
			$orderStr = [];
			
			if(is_array($order)){
				foreach($order as $k => $v){
					$orderStr[] = self::escapeIdentifier($k) . ' ' . self::dirBuilder($v);
				}
			}elseif(is_string($order)){
				$orderStr[] = $order;
			}
			
			$orderReturn = ' ORDER BY ' . implode(',', $orderStr);
		}
		
		return $orderReturn;
	}
	
	private static function dirBuilder($dir): string
	{
		if(is_string($dir)){
			return ['asc' => 'ASC', 'desc' => 'DESC'][strtolower($dir)] ?? $dir;
		}
		
		if(is_numeric($dir)){
			return [1 => 'ASC', 0 => 'DESC'][max(0, min(1, ((int) $dir)))] ?? $dir;
		}
		
		if(is_bool($dir)){
			return [true => 'ASC', false => 'DESC'][$dir];
		}
		
		return (string) $dir;
	}
	
	private static function limitBuilder($entries, $page): string
	{
		return ($entries > 0? ' LIMIT ' . $entries * $page . ', ' . $entries: '');
	}
	
	private static function escapeIdentifier(string $collumn): string
	{
		return implode('.', array_map(function($coll){
			return \dibi::getConnection()->getDriver()->escapeIdentifier($coll);
		}, explode('.', $collumn)));
	}
	
}